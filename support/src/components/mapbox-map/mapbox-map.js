import React, { useState, useRef } from 'react';
import ReactMapGL from 'react-map-gl';
import throttle from 'lodash/throttle';
import mapStyle from './style-spec.json';
import ablyService from '../../services/ably';

const MapboxToken = process.env.MAPBOX_API_KEY;
const initialMapState = {
  zoom: 14,
  latitude: -6.1646383,
  longitude: 39.1986324
};

const MapboxMap = () => {
  const [mapState, setMapState] = useState(initialMapState);
  const throttledUpdate = useRef(
    throttle((viewportState) => {
      ablyService.send(viewportState);
    }, 1000)
  ).current;

  const viewportChangeHandler = (viewport) => {
    const shareOptions = {
      latitude: viewport.latitude,
      longitude: viewport.longitude,
      zoom: viewport.zoom,
      bearing: viewport.bearing,
      pitch: viewport.pitch,
    };
    setMapState({...viewport});
    throttledUpdate(shareOptions);
  };

  return (
    <div style={{width: '100vw', height: '100vh'}}>
      <ReactMapGL
        {...mapState}
        mapStyle={mapStyle}
        width="100%"
        height="100%"
        mapboxApiAccessToken={MapboxToken}
        onViewportChange={viewportChangeHandler}
      />
    </div>
  );
}

export default MapboxMap;