import React from 'react';

import Layout from '../components/layout';
import MapboxMap from '../components/mapbox-map/mapbox-map';

const IndexPage = () => (
  <Layout>
    <div style={{ maxWidth: `300px`, marginBottom: `1.45rem` }}>
      <MapboxMap />
    </div>
  </Layout>
)

export default IndexPage
