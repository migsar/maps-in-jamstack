# Maps in JAMstack

This is a repository for the talk Maps in JAMstack. Here we explore some topics related to using maps with JAMstack.

Topics

* Map services overview
* Tiles
* Third party services
* Limited and offline maps
* Static, serverless and managed infrastructure

## Slides (Spectacle)
The slides include all the information and a map demo, this is a regular online map.

## Support (Gatsby)
The gatsby app is a very simple one-page PWA that allow users to interact with the map in the slides. It is a simple interface that is processed through a serverless function.

## Interaction pipeline

1. User gets a link in the slides, it has a session id.
2. User opens support site with the session id in the query.
3. User input is processed in a serverless function, checks for id to be valid, otherwise don't do anything.
4. Both apps are updated.
