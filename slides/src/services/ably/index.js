import { Realtime } from 'ably';

class AblyService {
  constructor(options) {
    if (options.token) {
      this.client = new Realtime(options.token);
      this.channelName = options.channel || 'default';
      this.subscriptors = [];

      this.init();
    } else {
      throw new Error('No token error');
    }
  }

  init() {
    const { client, channelName } = this;

    if (!(client && channelName)) {
      return;
    }

    client.connection.on('connected', () => {
      console.log('Connected to ably!');
      const channel = this.channel = client.channels.get(channelName);

      channel.subscribe((message) => {
        const { subscriptors } = this;
        
        subscriptors.forEach(subscriptor => {
          subscriptor(message);
        });
      })
    });
  }

  send(message) {
    const { channel } = this;

    if (!channel) {
      console.log('Service not initialized.')
      return;
    }

    channel.publish('test', message)
  }

  subscribe(subscriptor) {
    this.subscriptors.push(subscriptor);
  }
}

const options = {
  token: process.env.ABLY_TOKEN,
  channel: process.env.ABLY_CHANNEL,
}
const service = new AblyService(options);

export default service;