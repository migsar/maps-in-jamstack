import React, { createElement } from 'react';
import { render } from 'react-dom';
import { MDXProvider } from '@mdx-js/react';
import {
  Deck,
  Slide,
  Notes,
  FlexBox,
  Box,
  Progress,
  FullScreen,
  UnorderedList,
  Link,
  Heading,
  mdxComponentMap
} from 'spectacle';
import styled from '@emotion/styled';

// SPECTACLE_CLI_MDX_START
import slides, { notes } from './slides.mdx';
// SPECTACLE_CLI_MDX_END

// SPECTACLE_CLI_THEME_START
const theme = {
  colors: {
    primary: '#ffffff',
    secondary: '#ffffff',
    tertiary: '#647dd1',
    quaternary: '#ffc951',
    quinary: '#8bddfd'
  },
  fonts: {
    header: '"Helvetica Neue", Helvetica, Arial, sans-serif',
    text: 'Montserrat, Arial, sans-serif',
    monospace: '"Consolas", "Menlo", monospace'
  },
  fontSizes: {
    h1: '52px',
    h2: '44px',
    h3: '40px',
    h4: '32px',
    text: '36px',
    monospace: '20px'
  },
};
// SPECTACLE_CLI_THEME_END

// SPECTACLE_CLI_TEMPLATE_START
const template = () =>
  createElement(
    FlexBox,
    {
      justifyContent: 'space-between',
      position: 'absolute',
      bottom: 0,
      width: 1
    },
    [
      createElement(
        Box,
        {
          padding: 10,
          key: 'progress-templ'
        },
        createElement(Progress)
      ),
      createElement(
        Box,
        {
          padding: 10,
          key: 'fullscreen-templ'
        },
        createElement(FullScreen)
      )
    ]
  );
// SPECTACLE_CLI_TEMPLATE_END

// SPECTACLE_CLI_AUTOLAYOUT_START
const autoLayout = false;
// SPECTACLE_CLI_AUTOLAYOUT_END
const StyledList = styled(UnorderedList)`
list-style: none inside none;
margin-top: 4em;
display: inline-block;
min-height: 50vh;
`;
const ListContainer = styled('div')`
  display: flex;
  justify-content: space-around;
`;

const enhancedComponentMap = {
  ...mdxComponentMap,
  a: styled(Link)`
    text-decoration: none;
    color: #ffffff;
  `,
  ul: ({children, props}) => (
    <ListContainer>
      <StyledList {...props}>
        {children}
      </StyledList>
    </ListContainer>
  ),
};

const Wrapped = styled(({children: MDXSlide, ...props}) => <div {...props}><MDXSlide /></div>)`
  height: 70vh;
  display: flex;
  flex-flow: column;
  justify-content: space-between;
  `; 
const MDXSlides = () =>
  createElement(
    Deck,
    {
      autoLayout,
      loop: false,
      theme,
      template
    },
    slides
      .map((MDXSlide, i) => [MDXSlide, notes[i]])
      .map(([MDXSlide, MDXNotesForSlide], i) =>
        createElement(
          Slide,
          {
            key: `slide-${i}`,
            slideNum: i,
          },
          createElement(
            MDXProvider,
            {
              components: enhancedComponentMap
            },
            createElement(Wrapped, {
              children: MDXSlide,
            }),
            createElement(Notes, null, createElement(MDXNotesForSlide, null))
          )
        )
      )
  );

render(createElement(MDXSlides, null), document.getElementById('root'));
