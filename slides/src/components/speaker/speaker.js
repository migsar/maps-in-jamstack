import React from 'react';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';

const PersonalImg = styled((props) => <img {...props} />)`
  width: 240px;
  border-radius: 50%;
`;

const CompanyImg = styled((props) => <img {...props} />)`
  width: 240px;
`;

const Speaker = ({ className, name, image, company, companyLogo }) =>  (
  <div className={className}>
    <PersonalImg src={image} />
    <h3>{name} - Fullstack Developer</h3>
    {companyLogo ? <CompanyImg src={companyLogo} /> : <h2>{company}</h2>}
  </div>
);

Speaker.propTypes = {
  name: PropTypes.string.isRequired,
  image: PropTypes.string,
  company: PropTypes.string,
  companyLogo: PropTypes.string,
};

Speaker.defaultProps = {
  image: null,
  company: null,
  companyLogo: null,
};

const styledSpeaker = styled(Speaker)`
  display: inline-block;
`;

const Speakers = ({ className, children }) => {
  return (
    <div className={className}>
      { children }
    </div>
  );
}

const styledSpeakers = styled(Speakers)`
  display: flex;
  justify-content: space-around;
  margin-top: 3rem;
  font-family: Arial;
  font-size: 40px;
  text-align: center;
`;

export default Speaker;
export {
  styledSpeaker as Speaker,
  styledSpeakers as Speakers
}