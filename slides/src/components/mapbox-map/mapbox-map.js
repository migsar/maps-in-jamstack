import React, { useState, useEffect, useRef } from 'react';
import ReactMapGL from 'react-map-gl';
import throttle from 'lodash/throttle';
import mapStyle from './style-spec.json';
import ablyService from '../../services/ably';

const MapboxToken = process.env.MAPBOX_API_KEY;
const initialMapState = {
  zoom: 14,
  latitude: -6.1646383,
  longitude: 39.1986324
};

export const MapboxMap = () => {
  const [mapState, setMapState] = useState(initialMapState);
  const throttledUpdate = useRef(
    throttle((viewportState) => {
      setMapState({mapState, ...viewportState});
      console.log();
    }, 1000)
  ).current;

  useEffect(() => {
    ablyService.subscribe((viewportMessage) => {
      const { data: newViewport } = viewportMessage;
      throttledUpdate(newViewport);
    });
  });


  const viewportChangeHandler = (viewport) => setMapState({...viewport});

  return (
    <div style={{width: '100%', height: '100vh'}}>
      <ReactMapGL
        {...mapState}
        mapStyle={mapStyle}
        width="100%"
        height="100%"
        mapboxApiAccessToken={MapboxToken}
        onViewportChange={viewportChangeHandler}
      />
    </div>
  );
}

export default MapboxMap;